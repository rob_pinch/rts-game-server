package nl.pinch.gameserver.data

import com.google.gson.Gson

object Serializer {

    private val gson: Gson by lazy { Gson() }

    fun toJson(obj: Any) = gson.toJson(obj)
}