package nl.pinch.gameserver

object Settings {
    const val Port = 22222
    const val PacketSize = 20000

    const val AlignmentDistance = 5f
    const val CohesionDistance = 5f
    const val SeparationDistance = 3.5f

    const val UnitTargetOffset = 5f

    const val TickMs = 10L
    const val SocketTimeout = 30L

    val Charset = Charsets.US_ASCII
}

object Commands {
    const val Connect = "connect"
    const val Disconnect = "disconnect"
    const val Spawn = "spawn"
    const val Reset = "reset"

    const val MoveTo = "moveTo: "
}