package nl.pinch.gameserver.model

class MoveCommand(
        val unitIds: Array<String> = arrayOf(),
        val x: Float,
        val z: Float
)