package nl.pinch.gameserver.model

import nl.pinch.gameserver.Settings
import java.util.*

class GameUnit(val id: String = Random().nextInt().toString(),
               val ownerId: Int,
               var angle: Float,
               val position: Vector2D = Vector2D(),
               @Transient val previousPosition: Vector2D = Vector2D(),
               var targetPosition: Vector2D? = null,
               var groupId: Int? = null) {

    @Transient
    var velocity = Vector2D()

    @Transient
    var collisionThreshold = 3f
        set(value) {
            field = if (value < 0) 0f else value
        }

    fun reduceCollisionThreshold() {
        collisionThreshold -= .1f
    }

    fun restoreCollisionThreshold() {
        collisionThreshold = 3f
    }

    override fun equals(other: Any?): Boolean {
        return (other as? GameUnit)?.let {
            it.id == id
        } ?: false
    }

    override fun hashCode() = id.hashCode()

    companion object {
        const val speed = .1f
    }
}

fun GameUnit.move(units: List<GameUnit>) {
    if (reachedDestination(units)) {
        targetPosition = null
        velocity = Vector2D()
        restoreCollisionThreshold()
        return
    }

    targetPosition?.let { moveTo(units, it.x, it.y) }
}

/** Flocking using alignment, cohesion and separation */
fun GameUnit.moveTo(units: List<GameUnit>, x: Float, z: Float) {
    val mathAngle = (Math.atan2((z - position.y).toDouble(), (x - position.x).toDouble()) * 180 / Math.PI)

    val direction = Vector2D.toCartesian(GameUnit.speed, Math.toRadians(mathAngle).toFloat())
    val alignment = Vector2D()
    var cohesion = Vector2D()
    val separation = Vector2D()

    var nAlignment = 0
    var nCohesion = 0
    var nSeparation = 0

    for (unit: GameUnit in units) {
        if (unit.id != id
                && position.distance(unit.position) < Settings.AlignmentDistance
                && unit.groupId == groupId) {
            alignment.x += unit.velocity.x
            alignment.y += unit.velocity.y
            nAlignment++
        }

        if (unit.id != id
                && position.distance(unit.position) < Settings.CohesionDistance
                && unit.groupId == groupId) {
            cohesion.x += unit.position.x
            cohesion.y += unit.position.y
            nCohesion++
        }

        if (unit.id != id
                && position.distance(unit.position) < Settings.SeparationDistance) {
            separation.x += unit.position.x - position.x
            separation.y += unit.position.y - position.y
            nSeparation++
        }
    }

    if (nAlignment > 0) {
        alignment.normalize(nAlignment.toFloat())
        alignment.normalize()
    }

    if (nCohesion > 0) {
        cohesion.normalize(nCohesion.toFloat())
        cohesion = Vector2D(cohesion.x - position.x, cohesion.y - position.y)
        cohesion.normalize()
    }

    if (nSeparation > 0) {
        separation.normalize(nSeparation.toFloat())
        separation.reverse()
        separation.normalize()
    }

    velocity.x += direction.x * 0.8f + alignment.x * 0.15f + cohesion.x * .05f + separation.x * .18f
    velocity.y += direction.y * 0.8f + alignment.y * 0.15f + cohesion.y * .05f + separation.y * .18f

    velocity.normalize()
    velocity.normalize(1 / GameUnit.speed)
    angle = Math.toDegrees(velocity.angle.toDouble()).toFloat()

    previousPosition.x = position.x
    previousPosition.y = position.y

    position.x += velocity.x
    position.y += velocity.y
}

fun GameUnit.reachedDestination(units: List<GameUnit>): Boolean {
    return targetPosition?.let {
        if (!withinTargetDestination(units)) {
            // keep moving if not within destination radius
            false
        } else {
            // keep moving until started to move away from destination
            (previousDistanceToTarget() < distanceToTarget()) && !hasCollisions(units)
        }
    } ?: !hasCollisions(units)
}

fun GameUnit.withinTargetDestination(units: List<GameUnit>): Boolean {
    return targetPosition?.let {
        val groupSize = units.count { it.groupId == groupId }
        Math.abs(it.x - position.x) < Settings.UnitTargetOffset + (groupSize / 1.5f) && Math.abs(it.y - position.y) < Settings.UnitTargetOffset + (groupSize / 1.5f)
    } ?: true
}

fun GameUnit.distanceToTarget(): Float {
    return targetPosition?.let {
        position.distance(it)
    } ?: 0f
}

fun GameUnit.previousDistanceToTarget(): Float {
    return targetPosition?.let {
        previousPosition.distance(it)
    } ?: 0f
}

fun GameUnit.hasCollisions(units: List<GameUnit>): Boolean {
    units.forEach {
        if (it.id != id) {
            if (it.position.distance(position) < collisionThreshold) {
                reduceCollisionThreshold()
                return true
            }
        }
    }

    return false
}