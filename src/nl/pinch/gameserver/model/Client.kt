package nl.pinch.gameserver.model

import java.net.InetAddress
import java.util.Objects

class Client(val address: InetAddress, val port: Int) {

    val id: Int by lazy { address.hostAddress.hashCode() }

    override fun equals(other: Any?): Boolean {
        return (other as? Client)?.let {
            it.address.hostAddress == address.hostAddress
        } ?: false
    }

    override fun hashCode() = Objects.hashCode(address.hostAddress)
}