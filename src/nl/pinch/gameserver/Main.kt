package nl.pinch.gameserver

import com.google.gson.Gson
import nl.pinch.gameserver.game.Game
import nl.pinch.gameserver.model.*
import java.io.IOException
import java.net.DatagramPacket
import java.net.DatagramSocket
import java.util.*
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit


object Main {

    @JvmStatic
    fun main(args: Array<String>) {
        val app = App(Settings.Port)

        while (true) {
            val command = Scanner(System.`in`).next()

            when (command) {
                "reset" -> app.reset()
                "exit" -> System.exit(1)
            }
        }
    }
}

class App(port: Int) {

    private val server = Server(port)
    private val clients = ConcurrentHashMap<String, Client>()
    private val game = Game()
    private val gson = Gson()

    init {
        Thread {
            while (true) {
                server.readPacket()?.let { this@App.proccessPacket(it) }
            }
        }.start()

        schedule({
            clients.forEach { client ->
                game.serializeState().forEach { json ->
                    server.sendPacket(packet(client.value, json))
                }
            }
        }, Settings.SocketTimeout)
    }

    fun reset() {
        game.reset()
        println("Game reset")
    }

    fun proccessPacket(packet: DatagramPacket) {
        val data = packet.read()

        when (data) {
            Commands.Connect -> clients[packet.address.hostAddress] = Client(packet.address, packet.port)
            Commands.Disconnect -> clients.remove(packet.address.hostAddress)
            Commands.Reset -> game.reset()
            else -> {
                val client = clients[packet.address.hostAddress] ?: return

                when (data) {
                    Commands.Spawn -> { game.spawnUnit(client) }
                    else -> {
                        if (data.startsWith(Commands.MoveTo)) {
                            val moveCmd = gson.fromJson(data.drop(Commands.MoveTo.length), MoveCommand::class.java)
                            game.moveUnits(moveCmd)
                        }
                    }
                }
            }
        }
    }
}

class Server(port: Int) {

    private var serverSocket = DatagramSocket(port)
    private var packet: DatagramPacket? = null
    private var data: ByteArray? = null

    /** Blocking safe read of a packet */
    fun readPacket(): DatagramPacket? {
        data = ByteArray(Settings.PacketSize)
        packet = DatagramPacket(data!!, data!!.size)

        return try {
            serverSocket.receive(packet)
            packet
        } catch (e: IOException) {
            System.err.println("Error: error while receiving packet: ${e.message}")
            null
        }
    }

    /** Safe send of a packet */
    fun sendPacket(packet: DatagramPacket) {
        try {
            serverSocket.send(packet)
        } catch (e: IOException) {
            System.err.println("Error: error while sending packet: ${e.message}")
        }
    }
}

fun DatagramPacket.read() = String(data, 0, length, Settings.Charset)

fun packet(client: Client, data: String): DatagramPacket {
    val bytes = data.toByteArray()
    return DatagramPacket(bytes, bytes.size, client.address, client.port)
}

fun schedule(action: () -> Unit, delay: Long, poolSize: Int = 4) {
    Executors.newScheduledThreadPool(poolSize).scheduleAtFixedRate({
        action()
    }, 0, delay, TimeUnit.MILLISECONDS)
}