package nl.pinch.gameserver.game

import nl.pinch.gameserver.Settings
import nl.pinch.gameserver.data.Serializer
import nl.pinch.gameserver.model.*
import nl.pinch.gameserver.schedule
import java.util.*
import java.util.concurrent.CopyOnWriteArraySet

class Game {

    private val units = CopyOnWriteArraySet<GameUnit>()

    init {
        schedule({
            units.forEach { it.move(units.toList()) }
        }, Settings.TickMs)
    }

    fun reset() = units.clear()

    fun spawnUnit(client: Client) {
        units.add(GameUnit(ownerId = client.id, angle = 0f))
        println("Units count: ${units.size}")
    }

    fun moveUnits(command: MoveCommand) {
        val group = Random().nextInt()

        command.unitIds.forEach { id ->
            getUnit(id)?.apply {
                targetPosition = Vector2D(command.x, command.z)
                groupId = group

                // to reset reached destination logic
                previousPosition.x = position.x
                previousPosition.y = position.y
            }
        }
    }

    fun serializeState() = units.chunked(20).map { Serializer.toJson(it) }

    fun getUnit(id: String) = units.find { it.id == id }
}